export default (editor, config = {}) => {
    let pm=editor.Panels;
    pm.getButton("options", "sw-visibility").attributes.attributes.title = "顯示元件邊界";
    pm.getButton("options", "preview").attributes.attributes.title = "預覽";
    pm.getButton("options", "fullscreen").attributes.attributes.title = "全熒幕";
    pm.getButton("options", "export-template").attributes.attributes.title = "顯示程式";
    pm.getButton("views", "open-sm").attributes.attributes.title = "樣式";
    pm.getButton("views", "open-tm").attributes.attributes.title = "屬性";
    pm.getButton("views", "open-layers").attributes.attributes.title = "圖層";
    pm.getButton("views", "open-blocks").attributes.attributes.title = "元件";
    pm.addButton('options', {
        id: 'modify-head',
        className: 'fa fa-puzzle-piece',
        command: e => e.runCommand('modify-head'),
        attributes: {
            title: '修改head'
        },
        active: false,
    });
    pm.addButton('options', {
        id: 'canvas-clear',
        className: 'fa fa-trash',
        command: e => {
            e.runCommand('core:canvas-clear');
            editor.head=`
            <link rel="stylesheet" href="./css/style.css">
            <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js'></script>
            <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js'></script>
            <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js'></script>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
            `;
        },
        attributes: {
            title: '清空'
        },
        active: false,
    });
}