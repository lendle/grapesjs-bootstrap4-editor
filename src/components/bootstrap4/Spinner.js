import _s from "underscore.string";
import * as contextUtils from './contextUtils'

export default (editor, config = {}) => {
    let dc = editor.DomComponents;
    let bm = editor.BlockManager;
    var defaultType = dc.getType('defaultEx');
    var defaultModel = defaultType.model;
    var defaultView = defaultType.view;

    dc.addType('spinner', {
        model: defaultModel.extend({
            init: function(){
                this.context="text-dark";

                this.listenTo(this, 'change:Context', function(){
                    contextUtils.removeAllContextClasses(this.view.el, "text");
                    if(this.changed['Context']){
                        this.context=this.changed.Context;
                        this.refresh();
                    }
                });
            },
            defaults: Object.assign({}, defaultModel.prototype.defaults, {
                'custom-name': 'spinner',
                /*draggable: true,*/
                droppable: false,
                classes:[
                    "spinner-border"
                ],
                traits: [
                    {
                        type: 'select',
                        options: contextUtils.getContextOptionsList("text"),
                        label: 'Context',
                        name: 'Context',
                        changeProp: 1
                    }
                ].concat(defaultModel.prototype.defaults.traits)
            }),
            afterChange(e) {
            }
        }, {
                isComponent(el) {
                    if (el && el.classList && el.classList.contains('spinner-border')) {
                        return { type: 'spinner-border' };
                    }
                }
            }),
        view: {
            render: function () {
                defaultView.prototype.render.apply(this, arguments);
                $(this.el).addClass("spinner-border");
                $(this.el).addClass(this.model.context);
                //$(this.el).html("Spinner will display at runtime.");
                //$(this.el).html("123");
                //$(this.el).html("<div class='spinner-border "+this.model.context+"' style='width: "+this.model.width+"%'></div>");
                return this;
            }
        }
    });

    //http://astronautweb.co/snippet/font-awesome/
  bm.add('spinner', {
    label: "Spinner",
    attributes: {class:'fa fa-repeat'},
    category: 'Bootstrap4',
    content: {
      type: 'spinner'
    }
  });
}
