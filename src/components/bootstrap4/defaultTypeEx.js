import _s from "underscore.string";
import * as contextUtils from './contextUtils'

export default (editor, config = {}) => {
    let dc = editor.DomComponents;
    let bm = editor.BlockManager;
    var defaultType = dc.getType('default');
    var defaultModel = defaultType.model;
    var defaultView = defaultType.view;

    dc.addType('defaultEx', {
        model: defaultModel.extend({
            refresh: function(){
                const coll = this.collection;
                const at = coll.indexOf(this);
                coll.remove(this);
                coll.add(this, { at });
            },
            toHTML: function () {
                let copy=this.view.el.cloneNode(true);
                $(copy).removeClass("gjs-comp-selected");
                $(copy).removeAttr("data-gjs-type");
                $(copy).removeAttr("data-highlightable");
                //copy.removeChild(copy.firstChild);
                return copy.outerHTML;
            },
            
            afterChange(e) {
            }
        }, {
                /*isComponent(el) {
                    if (el && el.classList && el.classList.contains('spinner-border')) {
                        return { type: 'spinner-border' };
                    }
                }*/
            }),
        view: {
            render: function () {
                defaultView.prototype.render.apply(this, arguments);
                //$(this.el).addClass("spinner-border");
                //$(this.el).addClass(this.model.context);
                //$(this.el).html("Spinner will display at runtime.");
                //$(this.el).html("123");
                //$(this.el).html("<div class='spinner-border "+this.model.context+"' style='width: "+this.model.width+"%'></div>");
                return this;
            }
        }
    });

    //http://astronautweb.co/snippet/font-awesome/
}
