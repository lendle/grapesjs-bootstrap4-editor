import Progress from "./Progress";
import Spinner from "./Spinner";
import Jumbotron from "./Jumbotron";
import ListGroup from "./ListGroup";
import ListGroupItem from "./ListGroupItem";
import Badge from "./Badge";
import DefaultTypeEx from "./defaultTypeEx";

export default (editor, config = {}) => {
  DefaultTypeEx(editor, config);
  Progress(editor, config);
  Spinner(editor, config);
  Jumbotron(editor, config);
  ListGroup(editor, config);
  ListGroupItem(editor, config);
  Badge(editor, config);
}
