import _s from "underscore.string";
import * as contextUtils from './contextUtils'

export default (editor, config = {}) => {
    let dc = editor.DomComponents;
    let bm = editor.BlockManager;
    var defaultType = dc.getType('defaultEx');
    var defaultModel = defaultType.model;
    var defaultView = defaultType.view;

    dc.addType('progress', {
        model: defaultModel.extend({
            
            init: function(){
                this.width=50;
                this.context="bg-primary";

                this.listenTo(this, 'change:Progress', function(a){
                    //$(this.view.el).find(".progress-bar").css("width", a.changed.Progress+"%");
                    this.width=a.changed.Progress;
                    this.refresh();
                });

                this.listenTo(this, 'change:Context', function(a){
                    contextUtils.removeAllContextClasses($(this.view.el).find(".progress-bar").get(0), "bg");
                    if(this.changed['Context']){
                        this.context=this.changed.Context;
                        this.refresh();
                    }
                });
            },
            defaults: Object.assign({}, defaultModel.prototype.defaults, {
                'custom-name': 'Progress',
                /*draggable: true,*/
                droppable: false,
                classes:[
                    "progress"
                ],
                traits: [
                    {
                        type: 'number',
                        label: 'Progress',
                        name: "Progress",
                        changeProp: 1
                    },
                    {
                        type: 'select',
                        options: contextUtils.getContextOptionsList("bg"),
                        label: 'Context',
                        name: 'Context',
                        changeProp: 1
                    }
                ].concat(defaultModel.prototype.defaults.traits)
            }),
            /*init2() {
              linkModel.prototype.init2.call(this); // call parent init in this context.
            },*/
            afterChange(e) {
            }
        }, {
                isComponent(el) {
                    if (el && el.classList && el.classList.contains('progress')) {
                        return { type: 'progress' };
                    }
                }
            }),
        view: {
            render: function () {
                defaultView.prototype.render.apply(this, arguments);
                $(this.el).html("<div class='progress-bar "+this.model.context+"' style='width: "+this.model.width+"%'></div>");
                return this;
            }
        }
    });

    bm.add('progress', {
        label: "Progress",
        attributes: {class:'fa fa-forward'},
        category: 'Bootstrap4',
        content: {
          type: 'progress'
        }
      });
}
