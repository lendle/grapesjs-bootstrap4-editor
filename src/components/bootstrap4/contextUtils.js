const defaultContextOptionsList = [
    { value: '', name: 'None' },
    { value: 'primary', name: 'Primary' },
    { value: 'success', name: 'Success' },
    { value: 'info', name: 'Info' },
    { value: 'warning', name: 'Warning' },
    { value: 'danger', name: 'Danger' },
    { value: 'white', name: 'White' },
    { value: 'secondary', name: 'Secondary' },
    { value: 'light', name: 'Light' },
    { value: 'dark', name: 'Dark' }
];

function getContextOptionsList(prefix, additionalList) {
    let newArray = null;
    newArray = [];
    for (let i of defaultContextOptionsList) {
        newArray.push({ ...i });
    }
    if (additionalList) {
        for (let i of additionalList) {
            newArray.push(i);
        }
        newArray;
    } 
    if (prefix) {
        for (let i of newArray) {
            if(i.value!=''){
                i.value = prefix + "-" + i.value;
            }
        }
    }
    return newArray;
}

function removeAllContextClasses(el, prefix, additionalList) {
    let array = getContextOptionsList(prefix, additionalList);
    for (let o of array) {
        if (o.value != '') {
            $(el).removeClass(o.value);
        }
    }
}

export { getContextOptionsList, removeAllContextClasses };