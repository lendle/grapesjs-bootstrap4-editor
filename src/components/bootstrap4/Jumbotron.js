import _s from "underscore.string";

export default (editor, config = {}) => {
    let bm = editor.BlockManager;
    bm.add('jumbotron', {
        label: "Jumbotron",
        attributes: {class:'fa fa-credit-card'},
        category: 'Bootstrap4',
        content: `
            <div class="jumbotron">
                <h1>The Header of Jumbotron</h1> 
                <p>The Body Text of Jumbotron</p> 
            </div>
        `,
      });
};