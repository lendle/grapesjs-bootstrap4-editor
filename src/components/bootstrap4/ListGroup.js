import _s from "underscore.string";
import * as contextUtils from './contextUtils'

export default (editor, config = {}) => {
    let bm = editor.BlockManager;
    let dc = editor.DomComponents;
    var defaultType = dc.getType('default');
    var defaultModel = defaultType.model;
    var defaultView = defaultType.view;

    dc.addType('listGroup', {
        model: defaultModel.extend({
            refresh: function(){
                const coll = this.collection;
                const at = coll.indexOf(this);
                coll.remove(this);
                coll.add(this, { at });
            },
            init: function () {
                this.horizontal=false;
                this.listenTo(this, 'change:horizontal', function(){
                    this.horizontal=this.changed.horizontal;
                    this.refresh();
                });
            },
            toHTML: function () {
                let copy=this.view.el.cloneNode(true);
                $(copy).removeClass("gjs-comp-selected");
                $(copy).removeAttr("data-gjs-type");
                $(copy).removeAttr("data-highlightable");
                return copy.outerHTML;
            },
            defaults: Object.assign({}, defaultModel.prototype.defaults, {
                'custom-name': 'listGroup',
                /*draggable: true,*/
                droppable: false,
                classes: [
                    "list-group"
                ],
                traits: [
                    {
                        type: 'checkbox',
                        label: 'Horizontal',
                        name: 'horizontal',
                        changeProp: 1
                    }
                ].concat(defaultModel.prototype.defaults.traits)
            }),
            afterChange(e) {
            }
        }, {
                isComponent(el) {
                    if (el && el.classList && el.classList.contains('list-group')) {
                        return { type: 'listGroup' };
                    }
                }
            }),
        view: {
            render: function () {
                defaultView.prototype.render.apply(this, arguments);
                if(this.model.horizontal){
                    $(this.el).addClass("list-group-horizontal");
                }else{
                    $(this.el).removeClass("list-group-horizontal");
                }
                return this;
            }
        }
    });

    bm.add('listGroup', {
        label: "ListGroup",
        attributes: {class:'fa fa-list-alt'},
        category: 'Bootstrap4',
        content: `
        <ul class="list-group">
            <li class="list-group-item">First item</li>
            <li class="list-group-item">Second item</li>
            <li class="list-group-item">Third item</li>
        </ul>
        `,
      });
};