import _s from "underscore.string";
import * as contextUtils from './contextUtils'

export default (editor, config = {}) => {
    let bm = editor.BlockManager;
    let dc = editor.DomComponents;
    var defaultType = dc.getType('defaultEx');
    var defaultModel = defaultType.model;
    var defaultView = defaultType.view;

    dc.addType('listGroupItem', {
        model: defaultModel.extend({
            init: function () {
                
            },
            defaults: Object.assign({}, defaultModel.prototype.defaults, {
                'custom-name': 'listGroupItem',
                //draggable: 'listGroupItem',
                droppable: false,
                classes: [
                    "list-group-item"
                ],
                traits: [
                ].concat(defaultModel.prototype.defaults.traits)
            }),
            afterChange(e) {
            }
        }, {
                isComponent(el) {
                    if (el && el.classList && el.classList.contains('list-group-item')) {
                        return { type: 'listGroupItem' };
                    }
                }
            }),
        view: {
            render: function () {
                defaultView.prototype.render.apply(this, arguments);
                
                return this;
            }
        }
    });

    bm.add('listGroupItem', {
        label: "ListGroupItem",
        attributes: {class:'fa fa-list-alt'},
        category: 'Bootstrap4',
        content: `
        <li class="list-group-item">Item</li>
        `,
      });
};