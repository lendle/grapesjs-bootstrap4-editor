import _s from "underscore.string";
import * as contextUtils from './contextUtils'

export default (editor, config = {}) => {
    let dc = editor.DomComponents;
    let bm = editor.BlockManager;
    var defaultType = dc.getType('defaultEx');
    var defaultModel = defaultType.model;
    var defaultView = defaultType.view;

    dc.addType('badge', {
        model: defaultModel.extend({
            init: function(){
                this.context="badge-dark";
                this.text="Badge";

                this.listenTo(this, 'change:Context', function(){
                    contextUtils.removeAllContextClasses(this.view.el, "badge");
                    if(this.changed['Context']){
                        this.context=this.changed.Context;
                        this.refresh();
                    }
                });

                this.listenTo(this, 'change:Text', function(){
                    if(this.changed['Text']){
                        this.text=this.changed.Text;
                        this.refresh();
                    }
                });
            },
            defaults: Object.assign({}, defaultModel.prototype.defaults, {
                'custom-name': 'badge',
                tagName: 'span',
                /*draggable: true,*/
                droppable: false,
                classes:[
                    "badge",
                    "badge-dark"
                ],
                traits: [
                    {
                        type: 'select',
                        options: contextUtils.getContextOptionsList("badge"),
                        label: 'Context',
                        name: 'Context',
                        changeProp: 1
                    },
                    {
                        type: 'text',
                        label: 'Text',
                        name: 'Text',
                        changeProp: 1
                    }
                ].concat(defaultModel.prototype.defaults.traits)
            }),
            afterChange(e) {
            }
        }, {
                isComponent(el) {
                    if (el && el.classList && el.classList.contains('badge')) {
                        return { type: 'badge' };
                    }
                }
            }),
        view: {
            render: function () {
                defaultView.prototype.render.apply(this, arguments);
                $(this.el).removeClass();
                $(this.el).addClass("badge");
                $(this.el).addClass(this.model.context);
                $(this.el).text(this.model.text);
                
                return this;
            }
        }
    });

    //http://astronautweb.co/snippet/font-awesome/
  bm.add('badge', {
    label: "Badge",
    attributes: {class:'fa fa-certificate'},
    category: 'Bootstrap4',
    content: {
      type: 'badge'
    }
  });
}
