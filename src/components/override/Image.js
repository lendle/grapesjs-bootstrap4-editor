import _s from "underscore.string";
import * as contextUtils from '../bootstrap4/contextUtils'

export default (editor, config = {}) => {
    let dc = editor.DomComponents;
    let bm = editor.BlockManager;
    var defaultType = dc.getType('default');
    var defaultModel = defaultType.model;
    var defaultView = defaultType.view;
    var imageType = dc.getType('image');
    var imageModel = imageType.model;
    var imageView = imageType.view;

    dc.addType('image', {
        model: imageModel.extend({
            init: function(){
                this.imgSrc="";
                this.listenTo(this, 'change:imgSrc', function(a){
                    this.imgSrc=a.changed.imgSrc;
                    const coll = this.collection;
                    const at = coll.indexOf(this);
                    coll.remove(this);
                    coll.add(this, { at });
                });
            },
            toHTML: function () {
                let copy=this.view.el.cloneNode(true);
                $(copy).removeClass("gjs-comp-selected");
                $(copy).removeAttr("data-gjs-type");
                $(copy).removeAttr("data-highlightable");
                return copy.outerHTML;
            },
            defaults: Object.assign({}, imageModel.prototype.defaults, {
                'custom-name': 'Image',
                /*draggable: true,*/
                resizable: 1,
                droppable: false,
                classes:[
                ],
                traits: [
                    {
                        type: 'text',
                        label: 'Src',
                        name: "imgSrc",
                        changeProp: 1
                    }
                ].concat(imageModel.prototype.defaults.traits)
            }),
            afterChange(e) {
            }
        }, {
                isComponent(el) {
                    if (el && el.tagName=="IMAGE") {
                        return { type: 'image' };
                    }
                }
            }),
        view: {
            render: function () {
                imageView.prototype.render.apply(this, arguments);
                $(this.el).attr("src", this.model.imgSrc);
                return this;
            }
        }
    });

    bm.add('image', {
        label: "Image",
        attributes: {class:'fa fa-picture-o'},
        category: 'Basic',
        content: {
          type: 'image'
        }
      });
}
