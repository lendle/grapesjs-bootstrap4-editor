import _ from 'underscore';
//import _s from 'underscore.string';
export default (editor, config = {}) => {

  const tm = editor.TraitManager;

  tm.addType("event_handler", {
    events:{
      'change': 'onChange',  // trigger parent onChange method on input change
    },
    getInputEl: function(){
      if (!this.inputEl) {
        var input = document.createElement('input');
        this.inputEl = input;
        var target=this.target;
        var model=this.model;
        var eventName=model.get('eventName');

        $(input).click(function(){
          //let dialogElement=document.getElementById("__codeEditorElement");
          let scriptText=null;
          let eventHandlerName=$(target.view.el).attr(eventName);
          if(eventHandlerName){
            eventHandlerName=eventHandlerName.substring(0, eventHandlerName.indexOf("("));
            let components=editor.getComponents();
            let scriptElement=null;
            let scriptModel=null;
            for(let model of components.models){
              if(model.ccid==eventHandlerName){
                scriptElement=model.view.$el;
                scriptModel=model;
              }
            }
            scriptModel.collection.remove(scriptModel);
            scriptText=$(scriptElement).text();
            scriptText=scriptText.substring(scriptText.indexOf("{")+1, scriptText.lastIndexOf("}"));
            //globalContext.__codeEditor.setValue(scriptText);
          }else{
            eventHandlerName="__"+eventName+(new Date().getTime());
            scriptText="";
            //globalContext.__codeEditor.setValue("");
            let attributes={};
            attributes[eventName]=eventHandlerName+"();";
            target.addAttributes(attributes);
          }
          __showCodeEditor(scriptText, "javascript", function(){
            editor.addComponents("<script id='"+eventHandlerName+"'>function "+eventHandlerName+"(){"+
            globalContext.__codeEditor.getValue()+"}</script>");
          });
          /*$(dialogElement).dialog({
            width: 800,
            height: 300,
            buttons: [
              {
                text: "OK",
                click: function(){
                  
                  $(this).dialog("close");
                }
              },
              {
                text: "Cancel",
                click: function(){
                  $(this).dialog("close");
                }
              }
            ]
          });*/
        });
      }
      return this.inputEl;
    }
  });
  // Select trait that maps a class list to the select options.
  // The default select option is set if the input has a class, and class list is modified when select value changes.
  tm.addType('class_select', {
    events:{
      'change': 'onChange',  // trigger parent onChange method on input change
    },
    getInputEl: function() {
      if (!this.inputEl) {
        var md = this.model;
        var opts = md.get('options') || [];
        var input = document.createElement('select');
        var target = this.target;
        var target_view_el = this.target.view.el;
        for(let i = 0; i < opts.length; i++) {
          let name = opts[i].name;
          let value = opts[i].value;
          if(value=='') { value = 'GJS_NO_CLASS'; } // 'GJS_NO_CLASS' represents no class--empty string does not trigger value change
          let option = document.createElement('option');
          option.text = name;
          option.value = value;
          const value_a = value.split(' ');
          //if(target_view_el.classList.contains(value)) {
          if(_.intersection(target_view_el.classList, value_a).length == value_a.length) {
            option.setAttribute('selected', 'selected');
          }
          input.append(option);
        }
        this.inputEl = input;
      }
      return this.inputEl;
    },

    onValueChange: function () {
      var classes = this.model.get('options').map(opt => opt.value);
      for(let i = 0; i < classes.length; i++) {
        if(classes[i].length > 0) {
          var classes_i_a = classes[i].split(' ');
          for(let j = 0; j < classes_i_a.length; j++) {
            if(classes_i_a[j].length > 0) {
              this.target.removeClass(classes_i_a[j]);
            }
          }
        }
      }
      const value = this.model.get('value');
      if(value.length > 0 && value != 'GJS_NO_CLASS') {
        const value_a = value.split(' ');
        for(let i = 0; i < value_a.length; i++) {
          this.target.addClass(value_a[i]);
        }
      }
      this.target.em.trigger('change:selectedComponent');
    }
  });

}
