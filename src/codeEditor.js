function __showCodeEditor(codeText, language="javascript", callback=null) {
    let dialogElement = document.getElementById("__codeEditorElement");
    monaco.editor.setModelLanguage(globalContext.__codeEditor.getModel(), language);
    globalContext.__codeEditor.setValue(codeText);
    $(dialogElement).dialog({
        width: 800,
        height: 300,
        buttons: [{
            text: "OK",
            click: function () {
                callback();
                // editor.addComponents("<script id='" + eventHandlerName + "'>function " + eventHandlerName + "(){" +
                //     globalContext.__codeEditor.getValue() + "}</script>");
                $(this).dialog("close");
            }
        },
        {
            text: "Cancel",
            click: function () {
                $(this).dialog("close");
            }
        }
        ]
    });
}